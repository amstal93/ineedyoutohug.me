import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Snackbar from '@material-ui/core/Snackbar'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import * as React from 'react'
import * as Reflux from 'reflux'
import './App.css'

class App extends Reflux.Component {
  public state = {
    is_near : false,
    message : "",
    response : ""
  }

  public render() {
    return (
      <React.Fragment>
        <div className="centered">
          <Typography variant="h4" align="center" gutterBottom={true}>
            I Need You To Hug.Me
          </Typography>

          <TextField
            label="Message (optional)"
            variant="outlined"
            multiline={true}
            rowsMax="2"
            className="message"
            onChange={this.handleMessageChange}
            margin="dense"
            value={this.state.message}
            helperText="Give me at least some context :)"
          />

          <FormControlLabel
            className="is_near"
            control={
              <Checkbox
                checked={this.state.is_near}
                onChange={this.handleIsNearChange}
                color="primary"
              />
            }
            label="I am near, look around!"
          />

          <Button variant="contained" color="primary" fullWidth={true} onClick={this.sendNotification}>
            Let Me Know You Need A Hug :3
          </Button>
        </div>

        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          open={Boolean(this.state.response)}
          autoHideDuration={8000}
          onClose={this.hideResponse}
          ContentProps={{
            'aria-describedby': 'response-message',
          }}
          message={<span id="response-message">{this.state.response}</span>}
        />
      </React.Fragment>
    )
  }

  private handleMessageChange = (event : React.ChangeEvent) => {
    const message = (event.target as TextFieldProps).value as string
    this.setState({message : (message.length > 64) ? message.substring(0, 64) : message})
  }

  private handleIsNearChange = (event: React.ChangeEvent, checked: boolean) => {
    this.setState({is_near : checked})
  }

  private hideResponse = () => this.setState({response : ""})

  private sendNotification = () => {
    let failed = false
    fetch('/api/notify', {
      body: "message=" + encodeURIComponent(this.state.message)
            + "&is_near=" + encodeURIComponent(this.state.is_near ? 1 : 0),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST'
    }).then(response => {
      failed = !response.ok
      if (response.status === 429) {
        return "Too Many Requests"
      }

      return response.text()
    }).then(responseText => {
      if (failed) {
        this.setState({response : responseText})
        return
      }

      this.setState({response : responseText, message : "", is_near : false})
    })
  }
}

export default App
